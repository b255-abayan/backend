console.log("Hello world!");

// Arithmetic Operator
let x = 1397;
let y = 7831;
let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of modulo operator: " + remainder);

// Assignment Operator
// Basic assignment operator (=)
let AssignmentNumber = 8;

// Addition assignment operator
AssignmentNumber = AssignmentNumber + 2;
console.log("Result of additional assignment operator:" + AssignmentNumber);

// Shorthand
AssignmentNumber += 2;
console.log("Result of additional assignment operator:" + AssignmentNumber);

AssignmentNumber -= 2;
console.log("Result of subtraction assignment operator:" + AssignmentNumber);

AssignmentNumber *= 2;
console.log("Result of multiplication assignment operator:" + AssignmentNumber);

AssignmentNumber /= 2;
console.log("Result of multiplication division operator:" + AssignmentNumber);


// Multiple Operators and Parenthesis
let mdas = 1 + 3 -3 *4 / 5;
console.log("Result of mdas operator:" + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operator:" + pemdas);

// Increment and Decrement
 let z = 1;
 let increment = ++z;
 console.log("Result of increment operator:" + increment);

 let decrement = --z;
 console.log("Result of decrement operator:" + decrement);

//  Type Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof(coercion));

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operator
let juan = "juan";

// Equality Operator (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);


// Inequality Operator
console.log('Start of Inequality Operator');
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);

// Strict Equality Operator
console.log('Start of Strict Equality Operator');
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Strict Inequality Operator
console.log('Start of Strict Inequality Operator');
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Relational Operator
console.log('Start of Relational Operator');
let a = 50;
let b = 65;

// GT Operator
let isGreaterThan = a > b;

// LT Operator
let isLessThan = a < b;

// GTE
let isGTorEqual = a >= b;

// LTE
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical Operator
console.log('Start of Logical Operator');
let isLegalAge = true;
let isRegistered = false;


// Logical AND Operator
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);


// Logical OR Operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet);

// Logical NOT Operator
let someRequirementsNotMet = !isRegistered;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet);