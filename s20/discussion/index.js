console.log("Hello world!");
// [SECTION] While loops
/*
  - A while loop takes in an expression/condition
  - Expressions are any unit of code that can be evaluated to a value

  
 */

let count = 5;
// While the value of count is not equal to 0
while(count !== 0){
    // The current value of count is printed out 
    console.log("While: " + count);
    // Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
    count--;
}

// [SECTION] Do while loop
/*
    - A do while loop works a lot likr the while loop. But unlike while loop, do-while loops guarantee that the code will be executed at least once.
*/

// let number = Number(prompt("Give me a number:"));

// do{
//     console.log("Do while: " + number);
//     number +=1;
// }while(number<10);


// [SECTION] For loops
/*
    - A for loop is more flexible that while and do-while loops
    1. The initialization value that will tract the progression of the loop.
    2. The expression/condition that will be evaluated which will determine wheter the loop will run one more time.
    3. The final expression indicates how to advance the loop
*/

// for(let count = 0; count <= 20; count++){
//     console.log(count);
// }


let myString = 'alex';
// Characters in strings may be counted using the .length property
console.log(myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[3]);

for(let x = 0; x < myString.length; x++){
    console.log(myString[x]);
}

// Mini-activity

// function printNumber(num){
//     for(let i = 1; i <= num; i++){
//         console.log(i);
//     }    
// }

// printNumber(10);


// let myName = 'AleX';
// for(let i = 0; i < myName.length; i++){
//     console.log(myName[i].toLocaleLowerCase());
//     if(

//         myName[i].toLocaleLowerCase() == 'a' ||
//         myName[i].toLocaleLowerCase() == 'e' ||
//         myName[i].toLocaleLowerCase() == 'i' ||
//         myName[i].toLocaleLowerCase() == 'o' ||
//         myName[i].toLocaleLowerCase() == 'u'
//     )
//         {
//             console.log(3);
//         }else{
//             console.log(myName[i]);
//         }
// }   


// [SECTION] Continue and Break Statement
/*
    - The continue statementt allows the code to go to the next iteration of the loop without finishing the execution of all statements
*/


for(let count = 0; count <= 20; count++){
    // If remainder is equal to 0
    if(count % 2 === 0){
        continue;
    }
    console.log("Continue and Break: " + count);
    if(count > 10 ){
        break;
    }
}


let names = 'alexandro';
for(let i = 0; i < names.length; i++){
    console.log(names[i]);
    if((names[i]).toLocaleLowerCase() === 'a'){
        console.log("Continue to the next iteration");
        continue;
    }

    if(names[i] == 'd'){
        break;
    }
}