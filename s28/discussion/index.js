// CRUD Operations

// Insert Documents (CREATE)
/*  Syntax:
    Insert One Document
    db.collectionaName.insertOne({
        "filedA": "valueA",
        "filedB": "valueB"
    })

    Insert Many Document
    db.collectionaName.insertMany([
        {
            "filedA": "valueA",
            "filedB": "valueB"
        },
        {
            "filedA": "valueA",
            "filedB": "valueB"
        }
    ])
 */

db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Doe",
    "age": 21,
    "email": "janedoe@mail.com",
    "company": "none"
});

db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "company": "none"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "company": "none"
    }
]);

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/

db.courses.insertMany([
    {
        "name": "Javascript 101",
        "price": 5000,
        "description": "Introduction to Javascript",
        "isActive": true
    },
    {
        "name": "HTML 101",
        "price": 2000,
        "description": "Introduction to HTML",
        "isActive": true
    },
    {
        "name": "CSS 101",
        "price": 2500,
        "description": "Introduction to CSS",
        "isActive": true
    }
]);


// Find Documents (Read/Retrieve)
db.users.find() //This will retrieve all the documents in the collection


db.users.find({
    "firstName": "Jane"
})

db.users.find({
    "firstName": "Neil",
    "age": 82
})

db.users.findOne({}); //Returns the first document in our collection

// Returns the first document that matches the criteria
db.users.findOne({
    "firstName": "Stephen",
});

// Update Documents (CREATE)

db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "age": 0,
    "email": "test@mail.com",
    "company": "none"
});

// Updating One Document
db.users.updateOne(
    {
    "firstName":"Bill"
    },
    {
        $set:{
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "email": "billgates@mail.com",
            "department": "Operations",
            "status": "active"
        }
    }
)

// Removing a field
db.users.updateOne(
    {
    "firstName": "Bill"
    },
    {
        $unset:{
            "status": "active"
        }
    }
)


// Updating multiple documents
db.users.updateMany(
    {
        "company":"none"
    },
    {
        $set:{
            "company": "HR"
        }
    }
)

// if no criteria, the first in document will take effect
db.users.updateOne(
    {

    },
    {
        $set:{
            "company": "Operations"
        }
    }
)

// if no criteria, all the document will take effect
db.users.updateMany(
    {

    },
    {
        $set:{
            "company": "comp"
        }
    }
)

// Deleting documents
db.users.insertOne({
    "firstName": "Test"
})

db.users.deleteOne({
    "firstName": "Test"
})

db.users.deleteMany({
    "company": "comp"
})

db.courses.deleteMany({}) //If not criteria, it will delete all documents