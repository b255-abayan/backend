const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");
// Creates an app variable that stores the result of the "express" function that initializes out express application
const app = express();

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://henry-255:admin123@zuitt-bootcamp.wydh8oh.mongodb.net/?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Defines the "/uses" string to be included for all user routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will not run the app directly
// else, if it is needed to be imported, it will not run the app and instead export 
if(require.main === module){
    // Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if non is defined
    // This syntax will allow flexibility when using the application locally or as a hosted application 
    app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
    });
}

module.exports = app;