const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
// S39 activity
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		courseController.addCourse(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});

// Route for retrieving all the courses
router.get("/all", (req, res) =>{
	courseController.getAllCourses(req.body).then(resultFromController =>
			res.send(resultFromController))
});

//Route for retrieving all the active courses
router.get("/", (req, res) =>{
	courseController.getAllActive(req.body).then(resultFromController =>
			res.send(resultFromController))
});

//Route for retrieving a specific courses
router.get("/:courseId", (req, res) =>{
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController =>
			res.send(resultFromController))
});

//Route for updating courses
router.put("/:courseId",auth.verify, (req, res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController =>
			res.send(resultFromController))
});

//Route for archieving courses
router.patch("/:courseId/archieve",auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		courseController.archieveCourse(req.params).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});




// Allows us to export the "router" object that will be acessed in our "index.js" file
module.exports = router;