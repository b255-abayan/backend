const Course = require("../models/Course");

// Create a new course
/*
    Steps:
    1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    2. Save the new Course to the database
*/

// S39 activity
module.exports.addCourse = (reqBody) => {
    // Create a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    let newCourse = new Course({
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    });

    // Saves the created object to our database
    return newCourse.save().then((course, error) => {
        // Course creation failed
        if(error) {
            return false;

        // Course creation successful
        } else {
            return true
        }
    })
}

module.exports.getAllCourses = () =>{
    return Course.find({}).then(result => {
        return result;
    })
}

//Retrieve all active courses
module.exports.getAllActive = () =>{
    return Course.find({isActive: true}).then(result => {
        return result;
    })
}

//Retrieve a specific courses
module.exports.getCourse = (reqParams) =>{
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
}


//Update courses
module.exports.updateCourse = (reqParams, reqBody) =>{
    
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,err) =>{
        if (err){
            return false;
        }else{
            return true;
        }
    })
}

//Archieving courses
module.exports.archieveCourse = (reqParams) =>{
    
    let updatedCourse = {
        isActive: false
    }
    return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,err) =>{
        if (err){
            return false;
        }else{
            return true;
        }
    })
}

// 