//Retrieve all do to list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map((item) => {
	return item.title;
})));


//Retrieve single do to list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


//Create a to do list item
fetch('https://jsonplaceholder.typicode.com/todos',{
    method: 'POST',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Created To Do List Item',
        completed: false,
        userId:1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


//Update a to do list item PUT
fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description:"To update the my to do list with a different data structure", 
        status: "Pending",
        title: 'Updated To Do List Item',
        userId:1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


//Update a to do list item PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: 'PATCH',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        status: "Complete",
        dateCompleted: "07/09/21"
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


//Delete to do list item
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});
