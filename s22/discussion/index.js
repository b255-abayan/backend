console.log("Hello World!");

// Array Methods
// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items

// Mutatator Methods
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];


console.log('Current array:');
console.log(fruits);

// push()
console.warn('push() - Add element at the end');
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated Array from push method:');
console.log(fruits);

// Adding multiple elements to an array

fruits.push('Avocado', 'Guava');
console.log(fruits);

// pop()
console.warn('pop() - Remove last element');
let removeFruit = fruits.pop();
console.log(removeFruit);
console.log('Mutated Array from pop method:');
console.log(fruits);

// unshift()
console.warn('unshift(arguments) - Add element at the beginning');
fruits.unshift('Lime', 'Banana');
console.log('Mutated Array from unshift method:');
console.log(fruits);

// shift()
console.warn('shift() - Remove element at the beginning');
let anotheFruit = fruits.shift();
console.log(anotheFruit);
console.log(fruits);

// splice()
console.warn('splice() - Remove element from specified index number and adds element');

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated Array from splice method:');
console.log(fruits);

// sort()
console.warn('sort() - Arrange elements into alphanumeric');
fruits.sort();
console.log('Mutated Array from sort method:');
console.log(fruits);


// reverse()
console.warn('reverse() - Reverses the order of array element');
fruits.reverse();
console.log('Mutated Array from reverse method:');
console.log(fruits);

console.warn('Non-Mutator');
let countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
console.warn('indexOf() - Returns the index of the first matching element found in an array. If no match found, the result will be -1');

let firstIndex = countries.indexOf('PH');
console.log('The result of indexOf method ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('The result of indexOf method ' + invalidCountry);

console.warn('lastIndexOf() - Returns the index of the last matching element found in an array. If no match found, the result will be -1');

let lastIndex = countries.lastIndexOf('PH');
console.log('The result of lastIndexOf method ' + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('The result of lastIndexOf method ' + lastIndexStart);

// slice()
console.warn('slice() - Portions/Slice elements from an array and return a new array');
console.log(countries);
let sliceArrayA = countries.slice(2);
console.log('The result of slice method: ');
console.log(sliceArrayA);

let sliceArrayB = countries.slice(2,4);
console.log(sliceArrayB);

let sliceArrayC = countries.slice(-3);
console.log(sliceArrayC);

// toString()
console.warn('toString()');
let stringArray = countries.toString();
console.log('The result of toString method: ');
console.log(stringArray);

// concat()
console.warn('concat() - Combines two arrays');
let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];
let tasks = taskArrayA.concat(taskArrayB);
console.log('The result of concat method: ');
console.log(tasks);

console.log('The result of concat method: ');
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log('The result of concat method: ');
console.log(combinedTasks);

// join()
console.warn('join() - Returns an array as a string separated by specified separator string');

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


// Iteration Methods
console.warn('Iteration Methods');

// forEach()
console.warn('forEach()');

allTasks.forEach(function(task){
    console.log(task);
});


let filteredTasks = [];
allTasks.forEach(function(task){
    if(task.length > 10){
        filteredTasks.push(task);
    }
});

console.log('The result of filteredTasks tasks: ');
console.log(filteredTasks);

// map()
console.warn('map() - Iterates on each element and return new array with different value depending on the result of the function operations');

let numbers = [1,2,3,4,5];
let numberMap = numbers.map(function(numbers){
    return numbers * numbers;
});
console.log('Original Array');
console.log(numbers);

console.log('Result of map method');
console.log(numberMap);

let numberForEach = numbers.forEach(function(num){
    return numbers * num;
});

console.log(numberForEach);

// every()
console.warn('every() - Checks if all elements in an array meet the given condition');

let allValid = numbers.every(function(number){
    return (number < 3);
});
console.log('Result of allValid method');
console.log(allValid);

// some()
console.warn('some() - Checks if atleast one element in the array meets the given condition');

let someValid = numbers.some(function(number){
    return (number < 2);
});
console.log('Result of someValid method');
console.log(someValid);


// filter()
console.warn('filter() - Returns new array that contains elements which meets the given condition');

let filterValid = numbers.filter(function(number){
    return (number < 3);
});
console.log('Result of filterValid method');
console.log(filterValid);


let nothingFound = numbers.filter(function(number){
    return (number = 0);
});

console.log('Result of nothingFound method');
console.log(nothingFound);

// Filtering using forEach()
let filteredNumbers = [];
numbers.forEach(function(number){
    if(number < 3){
        filteredNumbers.push(number);
    }
});
console.log('Result of filteredNumbers method');
console.log(filteredNumbers);

// includes()
console.warn('includes() - Check if the argument passed can be found in the array');

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes('Mouse');
console.log(productFound1);

let productFound2 = products.includes('Headset');
console.log(productFound2);

console.warn('chained methods');

let filteredProducts = products.filter(function(product){
    return product.toLowerCase().includes('a');
});
console.log(filteredProducts);

// reduce()
console.warn('reduce() - Evaluates elements from left to right and returns/reduces the array into a single value');

let iteration = 0;
let reduceArray = numbers.reduce(function(x,y){
    console.warn('Current Iteration: ' + ++iteration);
    console.warn('accumulator: ' + x);
    console.warn('currentValue: ' + y);

    return x + y;
});

console.log('Result of reduceArray method ' + reduceArray);

// Reducing string array
let list = ['Hello', 'Again', 'World'];
let reduceJoin = list.reduce(function(x,y){
    return x + ' ' + y;
});

console.log('Result of reduceJoin method');
console.log(reduceJoin);