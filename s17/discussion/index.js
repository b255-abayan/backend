console.log("Hello World!");

// Functions
// Functions in javascript are lines/blocks of codes thet tell our device/application to perform a certain task

// Function Declaration
function printName(){
    console.log("My name is John");
}

// Function Invocation
printName();

// Function Declaration vs expression

declaredFunction();
function declaredFunction(){
    console.log("Hello world from declaredFunction");
}
declaredFunction();

// Function expression
// Anonymous function - a function without a name
let variableFunction = function(){
    console.log("Hello Again!");
}

variableFunction();

// Function expresstion of a named function
let funcExpression = function funcName(){
    console.log("Hello from the other side");
}

funcExpression();

// Reassign declared function and expression to a new anonymous function

declaredFunction = function(){
    console.log("updated declaredFunction");
}
declaredFunction();

funcExpression = function(){
    console.log("updated functionExpression");
}

funcExpression();

// Cannot re-assing a function expression initialized with const

const constantFunc = function(){
    console.log("Initialized with const!");
}

// constantFunc();
// constantFunc = function(){
//     console.log("Cannot be reassigned!");
// }

constantFunc();

// Function Scoping

// Type of scope
/*
1. local/block scope
2. global scope
3. function scope
*/

{
    let localVar = "Armando Perez";
}
let localVar = "Mr. Worldwide"
console.log(localVar);


// function scope
function showName(){
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showName();
    // console.log(functionVar);
    // console.log(functionConst);
    // console.log(functionLet);

// Nested Function
// You can create a function inside a function. This is called a nested function

function myNewFunction(){
    let name = "Jane";
    function nestedFunction(){
        let nestedName = "John";
        console.log(name);
    }
    // console.log(nestedName);
    nestedFunction();
}

myNewFunction();


// Function and Global Scoped Variables
// Global Scoped Variables

let globalName = "Alexandro";

function myNewFunction2(){
    let nameInside = "Renz";
    console.log(globalName);
}

myNewFunction2();
// console.log(nameInside);

// Using alert();

// alert("Hello World!");


function showSampleAlert(){
    alert("Hello User");
}

// showSampleAlert();

console.log("I will only log in the console when the alert is dismissed");

// Using prompt()
let samplePrompt = prompt("Enter your name:");
console.log("Hello, " + samplePrompt);

let sampleNullPrompt = prompt("Dont enter anything:");
console.log("Hello, " + sampleNullPrompt);


function printWelcomeMessage(){
    let firstName = prompt("Enter your firstname:");
    let lastName = prompt("Enter your lastname:");
    console.log("Hello, " + firstName + " " + lastName + "!");
    console.log("Welcome to my page!");
}

printWelcomeMessage();


// Function Naming convention
// Function names should be definitive of the task it will perform. It usually contains a verb.

function getCourses(){
    let courses = ["Science 101", "Math 101", "English 101"];
}

getCourses();

// Avoid generic name to avoid confusion

function get(){
    let name = "Jamie";
    console.log(name);
}

get();

// Avoid pointless and inappropriate function names

function foo(){
    console.log(25%5);
}
foo();

// Name your function in small caps. Follow by camelCase when naming variables and functions

function displayCarInfo(){
    console.log("Brand: Toyota");
    console.log("Type: Sedan");
    console.log("Price: 1,500,00");
    console.log();
}

displayCarInfo();