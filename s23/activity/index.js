// console.log("Hello world!");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

let trainer = {
    name: 'Ash Ketchum',
    age: 10,
    pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty']
    },
    talk: function(){
       return `${this.pokemon[0]} I choose you!`;
    }
}


// Initialize/add the given object properties and methods

// Check if all properties and methods were properly added
console.log(trainer);

// Access object properties using dot notation
console.log('Result of dot notation:');
console.log(trainer.name);

// Access object properties using square bracket notation
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log('Result of talk method');
console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level){
    // Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		target.health = target.health - this.attack;
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is reduced to ${target.health}`);
		
		if (target.health <= 0) {
			return target.faint();
		}
	};
	this.faint = function(){
		console.log(`${this.name} fainted`);
	}
}

// Create/instantiate a new pokemon
let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);

// Create/instantiate a new pokemon
let geodude = new Pokemon('Geodude', 8);
console.log(geodude);
// Create/instantiate a new pokemon
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

// Invoke the tackle method and target a different object
geodude.tackle(pikachu);
console.log(pikachu);

// Invoke the tackle method and target a different object
mewtwo.tackle(geodude);
console.log(geodude);


//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
