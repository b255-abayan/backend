console.log("Hello World!");

// [SECTION] Objects

/*
    - An object is a data type that used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In javascript, most core Javascript features like string and arrays are objects (String are a collection of characters and are represented in a "key:value" pair)
    - A "key" is also mostly referred to as a property of an object
    - Different data types may be stored in a objects property creating complex data structures
*/


let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
}

console.warn("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);
console.log(cellphone.manufactureDate);

// Creating objects using a constructor function

function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.warn("Result from creating objects using object constructor");
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.warn("Result from creating objects using object constructor");
console.log(myLaptop);

console.warn("Creating empty objects");
let computer = {};
let myComputer = new Object();


// [SECTION] Accessing Object properties

let array = [laptop, myLaptop];
// Maybe confused  for accessing array indexes
console.log(array[0]['name']);
// This tells us that array[0] is an object by using the dot notation
console.log(array[0].name);

// [SECTION] Initializing/Adding/Deleting/Reassigning Object Properties

let car = {};

car.name = 'Honda Civic';
console.warn("Result from adding properties using dot notation");
console.log(car);
console.log(car.name);


car['manufacture date'] = 2019;
console.warn("manufacture date");
console.log(car['manufacture Date']);
console.warn("Result from adding properties using square bracket notation");

console.log(car);

//delete
console.warn('delete');
delete car['manufacture date'];
console.warn("Result from deleting properties");
console.log(car);

//reassigning
console.warn('reassigning');
car.name = 'Dodge Charger R/T';
console.warn("Result from reassigning properties");
console.log(car);

// [SECTION] Object methods
let person = {
    name: 'John',
    talk: function(){
        console.log('Hello my name is ' + this.name);
    }
}
console.log(person);
console.warn("Result from object methods");
person.talk();


person.walk = function(){
    console.log(this.name + ' walk 25 steps forward');
}

// Adding methods to objects
person.walk();


// Mini-activity
person.hobby = function(){
    console.log(this.name + ' likes watching television');
}
person.hobby();


// Methods are useful for creating reuseable functions that perform tasks related to object

let friend = {
    firstName: 'John',
    lastName: 'Smith',
    address: {
        city: 'Austin',
        state: 'Texas'
    },
    emails: ['joe@mail.com', 'joesmith@email.com'],
    introduce: function(){
        console.log('Hello my name is '+ this.firstName + ' ' + this.lastName)
    }
}

friend.introduce();

// [SECTION] Real world application of objects
/*
    - Scenario
    1. We would like to create a game that would have several pokemon interact with each other
    2. Every pokemon would have the same set of stats, properties and function
*/

let myPokemon = {
    name: 'Pikachu',
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log('This pokemon tackled targetPokemon');
        console.log('targetPokemons health is now reduced to _targetPokemonhealth_');
    },
    faint: function(){
        console.log('pokemon fainted');
    }
}

console.log(myPokemon);


// Creating an object constructor instead will help with the process

function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;
    this.tackle = function(target){
        console.log(this.name + ' tackled ' + target.name);
        console.log('targetPokemons health is reduced to _targetPokemonhealth_');
    }
    
    this.faint = function(){
        console.log(this.name + ' fainted');
    }
}



let pikachu = new Pokemon('Pikachu', 16);
let rattata = new Pokemon('Rattata', 8);


// pikachu.tackle(rattata);