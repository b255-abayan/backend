const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Home Route
app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
});

// Users mock database
let users = [
    {
        username: "johndoe",
        password: "johndoe1234"
    }
];

// Get Users Route
app.get("/users", (req, res) => {
    res.send(users);
});

// Delete User Route
app.delete("/delete-user", (req, res) => {
    let message;
    if(users.length > 0){
        if(req.body.username !== ''){
            for(let i = 0; i <users.length; i++){
                if(req.body.username == users[i].username){
                    let userIndex = users.indexOf(req.body.username);
                    users.splice(userIndex,1);
                    message = `User ${req.body.username} has been deleted.`;
                    break;
                }else{
                    message = "User does not exist.";
                }
            }
        }
    }else{
        message = "No User found.";
    }
    res.send(message);
});


if(require.main === module){
    app.listen(port, () => console.log(`Server running at ${port}`));
}

module.exports = app;