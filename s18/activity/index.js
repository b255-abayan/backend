/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/

// Function 1:
function addNum(num1, num2){
	console.log(num1 + num2);
}
console.log("Displayed sum of 5 and 15");
addNum(5, 15);

// Function 2:
function subNum(num1, num2){
	console.log(num1 - num2);
}
console.log("Displayed difference of 20 and 5");
subNum(20, 5);

// Function 3:
function multiplyNum(num1, num2){
	return num1 * num2;
}
let product = multiplyNum(50, 10);
console.log("The product of 50 and 10");
console.log(product);

// Function 4:
function divideNum(num1, num2){
	return num1 / num2;
}
let quotient = divideNum(50, 10);
console.log("The quotient of 50 and 10");
console.log(quotient);

// Function 5:

function getCircleArea(radius){
	const pi = 3.14159265359;
	return (pi * (radius ** 2)).toFixed(2);
}
let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

// Function 6:
function getAverage(num1, num2, num3, num4){
	return (num1 + num2 + num3 + num4)/4;
}
let averageVar = getAverage(20, 40, 60, 80);
console.log("The average of 20, 40, 60 and 80:");
console.log(averageVar);

// Function 7:
function checkIfPassed(score, totalScore){
	let percentage = (score / totalScore) * 100;
	let isPassed = percentage > 75;
	return isPassed;
}
let isPassingScore = checkIfPassed(38, 50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);

//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
