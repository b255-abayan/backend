console.log("Hello world!");

// Function
// Parameters and argumemts
// Functions in javascript are lines/block of codes that tell our device/application to perform certain tasks when they are called.

// Functions are mostly created to create complicated task 


function printInput(){
    let nickname = prompt("Enter your nickname:");
    console.log("Hi "+ nickname);
}

// printInput();

// For other cases, functions can also process data diretly passed inti it instead of relygin on global variables and prompt()

function printName(name){
    console.log("My name is " + name);

}
printName("Juana");
printName("Juan");
printName(2);

// You can directectly pass data into the function. The function can then call/use that data which is referred as "name"

// name is a parameter
// A parameter acts as a named variable/container that exists only inside of a function provided to a function when it is called/invoked


// Variables can also be passed as an argument

let sampleVariable = "Yui";
printName(sampleVariable);

// function arguments cannot be used by a function if there are no parameters provided within the function

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as arguments
// Function parameters can also accepts other functionds as arguments
// 
// Some complex functions use other functions as arguments

function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
    argumentFunction();
}

// Adding and removing parenthesis impacts the output of javascript heavily
// When a function is used with parethesis is denotes invoking/calling a function
invokeFunction(argumentFunction);

// finding information about a function in the console.log()
console.log(argumentFunction);


// Using multiple parameters
function createFullName(firstName, middleName, lastName){
    console.log(firstName + " " + middleName + " " +lastName);
}

createFullName("Juan", "Dela", "Cruz");

// In javascript, providing more/less arguments that the expected parameters will not return an error

// Providing less arguments 
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Hello");

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameter names are just names to refer to the arguments. Even if we change the name of the parameters, the arguments will be received in the same order it was passed

// The return statement
// The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstName, middleName, lastName){
    return firstName + " " + middleName + " " +lastName;
    console.log("This message will not be printed");
}

let completeName = returnFullName("Jeffrey", "Amazon", "Bezos");
console.log(completeName);

// This way , a function is able to return a value we can further use/manipulate in our program istead of only printing/displaying it in the console

// Mini-activity
// Create a function that will add two numbers together
// 1

function add(num1, num2){
    return num1 + num2;
}

let sum = add(5,5);

console.log(sum);

// Notice that the console.log() after the return is no longer printed in the console. That is because ideally any line/block of code that comes after thr return statements is ignored because it ends the function execution

// In thus example, console.log() will print the returned value of the returnFullName() function

console.log(returnFullName(firstName, middleName, lastName));

// You can also create a variable inside the function to contain in the result return that variable instead

function returnAddress(city, country){
    let fullAddress = city + " , " + country;
    return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Cebu");

console.log(myAddress);

// On the otherhand, when a function only has console.log() to display its results it will return undefined instead

function printPlayerInfo(username, level, job){
    console.log("Username: " + username);
    console.log("Level: " + level);
    console.log("Job: " + job);
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);

// Returns undefined because printPlayerInfo returns nothing. It only console.logs the details 
// You canno save any value from printPlayerInfo() because it does not return anything


