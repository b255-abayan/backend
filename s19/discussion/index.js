// console.log("Hello World!");

// Conditional Statements allows us to control the flow of our program.It allows us to run a statement/instruction if a condition is met or run another seperate instruction if otherwise

// [SECTION] if, else if, else statement

let numA = -1;
if(numA < 0){
    console.log("Hello");
}
console.log(numA<0); 

numA = 0;
if(numA < 0){
    console.log("Hello Again if numA is 0!");
}
console.log(numA<0); 

let city = "New York";
if(city === "New York"){
    console.log("Welcome to New York City!");
}

// Else if clause

/*
    - Execute a statement if previous conditions are false and if the specified conditions is true
    - The else if clause is optional and can be added to capture additional conditions to change the flow of a program
*/ 

let numH = 1;
if(numA < 0){
    console.log("Hello");
}else if(numH > 0){
    console.log("World");
}

// We were able to run the else if statement after we evaluated that the if condition was failed

// If the if condition was passed and run, we will no longer evaluate the else if and end the process there.

numA = 1;
if(numA > 0){
    console.log("Hello");
}else if(numH > 0){
    console.log("World");
}

// else if statement no longer run because the if statement 

city = "Tokyo";
if(city === "New York"){
    console.log("Welcome to New York City");
}else if(city === "Tokyo"){
    console.log("Welcome to Tokyo, Japan");
}

// Since we failed the condition for the first if(), we went to the else if() and checked and passed that condition

// else statement
/*
    - Executes a statement if all other conditions are false
    -The else statement is optional and can be added to capture any other result to change the flow of the program
*/

if(numA < 0){
    console.log("Hello");
}else if(numA === 0){
    console.log("World");
}else{
    console.log("Error! NumA is not less than 0");
}

// Else statments should only be added if there is a preceeding if condition. Else statements by itself will not work, however, if statements will work even there is no else statement



// with functions
 let message = "No Message";
 console.log(message);

 function determineTyphoonIntensity(windSpeed){
    if(windSpeed < 30){
        return "Not a typhoon yet";
    }else if(windSpeed <= 61){
        return "Tropical depression detected";
    }else if(windSpeed >= 62 && windSpeed <= 88){
        return "Tropical storm detected";
    }else if(windSpeed >= 89 || windSpeed <= 117){
        return "Severe tropical storm detected";
    }else{
        return "Typhoon Detected";
    }
 }

 message = determineTyphoonIntensity(50);
 console.log(message);

//  Mini-activity
function oddEven(number){
    if(number % 2 === 0){
        return "Number is Even";
    }else{
        return "Number is Odd";
    }
}
console.log(oddEven(2));

// Truthy Examples


if(true){
    console.log("Truthy");
}

if(1){
    console.log("Truthy");
}

if([]){
    console.log("Truthy");
}

// Falsy Examples

if(false){
    console.log("Falsy");
}

if(0){
    console.log("Falsy");
}

if(undefined){
    console.log("Falsy");
}

// [SECTION] Conditional Ternary Operator

// Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of Ternary Operator: ${ternaryResult}`);

// Multi statement execution
let _name;

function isOfLegalAge(){
    names = 'John';
    return `You are of legal age limit`;
}

function isUnderAge(){
    _name = 'Jane';
    return `You are under legal age limit`;
}

// let age = parseInt(prompt("What is your age?"));
// console.log(age);
// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log(`Result of Ternary Operator in functions : ${legalAge} , ${_name}`);

// [SECTION] Switch statement

// let day = prompt("What day of the week is it today?").toLocaleLowerCase();
// console.log(day);

// switch(day){
//     case 'monday':
//         console.log("The color of the day is red");
//         break;
//     case 'tuesday':
//         console.log("The color of the day is orange");
//         break;  
//     case 'wednesday':
//         console.log("The color of the day is yellow");
//         break;  
//     case 'thursday':
//         console.log("The color of the day is green");
//         break;  
//     case 'friday':
//         console.log("The color of the day is blue");
//         break;  
//     case 'saturday':
//         console.log("The color of the day is indigo");
//         break;  
//     case 'sunday':
//         console.log("The color of the day is violet");
//         break;
//     default:
//         console.log("Please input valid day");
//         break;
// }

// [SECTION] Try-catch-finally

function showIntensityAlert(){
    try{
        alert(determineTyphoonIntensity(windSpeed));
    }catch(error){
        console.log(typeof error);
        console.warn(error.message);
    }finally{
        alert("Intensity updates will show new alert");
    }
}

showIntensityAlert(56);