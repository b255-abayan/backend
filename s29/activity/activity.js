// Find users with letter "s" or "d" in lastName
db.users.find(
    {
    $or: [{ "firstName": {$regex: 's',$options: '$i'}}, {"lastName": {$regex: 'd',$options: '$i'}}]
    },
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
)

// Find users with company none and age is greaterthan or equal to 70
db.users.find(
    {
    $and: [{ "company": 'none'}, {"age": {$gte: 70}}]
    }
)

// Find users with letter e in the firstname and age is lessthan or equal to 30
db.users.find(
    {
    $and: [{ "firstName": {$regex: 'e', $options: '$i'}}, {"age": {$lte: 30}}]
    }
)