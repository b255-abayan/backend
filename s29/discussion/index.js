// [SECTION] Comparison Operators

// $gt/$gteoperator

db.users.find({
    "age": {$gt:50}
})

db.users.find({
    "age": {$gte: 76}
})

// $lt/$lte operator

db.users.find({
    "age": {$lt: 50}
})

db.users.find({
    "age": {$lte: 50}
})


// $ne operator
// Allows us to find documents that have field number values not equal to a specified value
db.users.find({
    "age": {$ne: 82}
})

// $in operator
// Allow us to find documents with specific match criteria one field using different values
db.users.find({
    "lastName": {$in: ["Hawking", "Doe"]}
})

db.courses.find({
    "name": {$in: ['HTML 101', 'CSS 101']}
})

// [SECTION] Logical Query Operators

// $or operator
// Allow us to find documents that match a single criteria from multiple provided search criteria
db.users.find({
    $or: [{ "firstName": "Neil"}, {"age": 25}]
})

// $or and $gt combination
db.users.find({
    $or: [{ "firstName": "Neil"}, {"age": {$gt: 70}}]
})

// $and operator
// Allow us to find documents matching multiple criteria in a single field
db.users.find({
    $and: [{ "age": {$ne:82}}, {"age": {$ne: 76}}]
})

//[SECTION] Field Projection
// Exclusion: 0, Inclusion: 1
db.users.find(
    {
    firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        email: 1
    }
)

db.users.find(
    {
    firstName: "Jane"
    },
    {
        company: 0,
        email: 0
    }
)

// Suppressing the ID field
// Allows us to exclude the _id field when retrieving documents
// when using field projection, field inclusion and exclusion may not be used at the same time
// Excluding the _id field is the only exception to this

db.users.find(
    {
    firstName: "Jane"
    },
    {
        firstName: 1,
        lastName:1,
        company: 1,
        _id: 0
    }
)

db.users.insert({
    firstName: "John",
    lastName: "Smith",
    age: 21,
    contact: {
        phone: "87654321",
        email: "johnsmith@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});


db.users.find(
    {
    firstName: "John"
    },
    {
        firstName: 1,
        lastName:1,
        "contact.phone": 1
    }
)

// Suppresing specific fields in embedded documents

db.users.find(
    {
    firstName: "John"
    },
    {
        "contact.phone": 0
    }
)

// embedded array
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});


// Project specific array elements in the returned array

db.users.find(
    {
    "namearr":{
        namea: "juan"
        }
    }
    ,
    {
        namearr: {$slice: 1}
    }
)

// [SECTION] Evaluation Query Operator
// $regex operator

// Case sensitive
db.users.find(
    {
        "firstName": {$regex: 'N'}
    }
)


// Case insensitive
db.users.find(
    {
        "firstName": {$regex: 'j', $options: '$i'}
    }
)