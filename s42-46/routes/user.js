const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/user");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for setting user to admin(Admin Only)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.patch("/:userId/grant-admin",auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	/*
		Check if the user authenticated is Administrator.
		If not, do not allow the execution and throw a message "Only Administrator can grant admin access to a user"
	*/
	if(userData.isAdmin == true){
		userController.grantAdminAccess(req.params).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send({message: "Only Administrator can grant admin access"});
	}
});

// Route for getting users details
router.get("/", auth.verify, (req, res) => {
	userController.getAllUserDetails().then(resultFromController => res.send(resultFromController));
});




module.exports = router;