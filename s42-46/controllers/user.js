const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");

// User Registration
module.exports.registerUser = async (reqBody) =>{
	try{
		return await User.findOne({email : reqBody.email}).then(result => {
			// User does not exist
			if(result == null){
	
				let newUser = new User({
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email : reqBody.email,
					password : bcrypt.hashSync(reqBody.password, 10)
				})
				return newUser.save().then((user, error) =>{
					if(error) {
						return  {message : "Something wen't wrong"};
					} else {
						return  {message : "User created successfully"};
					};
				})
	
			// User exists
			} else {
				return  {message : "Account already registered"};
			}
		})
	}catch(err){
		return {"message": err.message}
	}
}

// User Login
module.exports.loginUser = async (reqBody) => {
	try{
		return await User.findOne({email : reqBody.email}).then(result => {
			// User does not exist
			if(result == null){
				return false;
			// User exists
			} else {
				// Creates the variable "isPasswordCorrect" to return the result of the comparing login form password and the database password
				// The "compareSync" method is used to compare a non encrypted password from the login from to the encrypted password retrieved from the database
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
	
				// If the passwords match/result of the above code is true
				if(isPasswordCorrect) {
					// Generate an access token
					// Uses the "createAccessToken" method defined in the "auth.js" file
					// Returning an object back to the frontend application is common practice to ensure information is properly labeled
					return {access : auth.createAccessToken(result)}
				// Passwords do not match
				} else {
					return  {access : "Authentication Failed"};
				}
			}
		})
	}catch(err){
		return {"message": err.message}
	}
}

// Set User as admin(Admin Only)
module.exports.grantAdminAccess = async(reqParams) => {
	try{
		// Define a value to be replaced on the record to make it admin
		let updatedUser = {
			isAdmin: true
		}
		return await User.findByIdAndUpdate(reqParams.userId,updatedUser).then((user,error) =>{
			/*
				Check if the user trying to update is found on the records.
				If not, do not allow the execution and throw a message "Sorry, the user you are trying to update is not found on our record"
			*/
			if(user){
				/*
					If the user found on record is already administrator, throw a message "User has administrator access already"
					If not, throw a message "User is now Administrator"
				*/
				if(user.isAdmin == true){
					return  {message : "User has administrator access already"};
				}else{
					if (error){
						return  {message : error.message};
					}else{
						return  {message : "User is now Administrator"};
					}
				}
			}else{
				return  {message : "Sorry, the user you are trying to update is not found on our record"};
			}
			
		})
	}catch(err){
		return {"message": err.message}
	}
	
}

// Get All User details
module.exports.getAllUserDetails = async () => {
	try{
		return await User.find({}).then(users =>{
			return users;
		})
	}catch(err){
		return {"message": err.message}
	}
	
}


