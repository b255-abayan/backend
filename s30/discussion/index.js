// Create documents to use for our discussion
db.fruits.insertMany([
    {
      name : "Apple",
      color : "Red",
      stock : 20,
      price: 40,
      supplier_id : 1,
      onSale : true,
      origin: [ "Philippines", "US" ]
    },
  
    {
      name : "Banana",
      color : "Yellow",
      stock : 15,
      price: 20,
      supplier_id : 2,
      onSale : true,
      origin: [ "Philippines", "Ecuador" ]
    },
  
    {
      name : "Kiwi",
      color : "Green",
      stock : 25,
      price: 50,
      supplier_id : 1,
      onSale : true,
      origin: [ "US", "China" ]
    },
  
    {
      name : "Mango",
      color : "Yellow",
      stock : 10,
      price: 120,
      supplier_id : 2,
      onSale : false,
      origin: [ "Philippines", "India" ]
    }
  ]);

//   [SECTION] MongoDB Aggregation

// Using the aggregate method
db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    {
        $group: { _id: "$supplier_id", total: {$sum:"$stock" }}
    }
])

// Field projection with aggregation
db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    {
        $group: { _id: "$supplier_id", total: {$sum:"$stock" }}
    },
    {
        $project: {
            _id: 0
        }
    }
])

// Sorting aggregated results

db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    {
        $group: { _id: "$supplier_id", total: {$sum:"$stock" }}
    },
    {
        $sort: {
            total: -1
        }
    }
])

// Aggregating results based on array fields

db.fruits.aggregate([
    {$unwind: "$origin" }
])

// Display fruit document by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
    {$unwind: "$origin" },
    {$group: { _id: "$origin", kinds: {$sum: 1}}}
])

// [SECTION] Guidelines on Schema Design

// One-to-one relationship

var owner  = ObjectId();
db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "12345678"
});

// Change the "owner_id" using the actual id in the previously created document
db.supplier.insert({
    name: "ABC Fruits",
    contact: "123456789",
    owner_id: owner
})


// One-To-Few Relationship
db.suppliers.insert({
    name: "DEF Fruits",
    contact: "1234567890",
    addresses : [
      { street: "123 San Jose St", city: "Manila"},
      { street: "367 Gil Puyat", city: "Makati"}
    ]
  });
  
  // One-To-Many Relationship
  var supplier = ObjectId();
  var branch1 = ObjectId();
  var branch2 = ObjectId();
  
  db.suppliers.insert({
    _id: supplier,
    name: "GHI Fruits",
    contact: "1234567890",
    branches: [
      branch1
    ]
  });
  
  // Change the "<branch_id>" and "<supplier_id>" using the actual ids in the previously created document
  db.branches.insert({
    _id: <branch_id>,
      name: "BF Homes",
      address: "123 Arcardio Santos St",
      city: "Paranaque",
      supplier_id: <supplier_id>
  });
  
  db.branches.insert(
    {
      _id: <branch_id>,
        name: "Rizal",
        address: "123 San Jose St",
        city: "Manila",
        supplier_id: <supplier_id>
    }
  );