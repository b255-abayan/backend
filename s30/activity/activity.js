// Fruits onSale
db.fruits.aggregate(
    [
        {
        $match: { onSale: true}
        },
        {
        $count: "fruitsOnSale"
        }
    ]
)

// Fruits with stock morethan or equal to 20
db.fruits.aggregate(
    [
        {
        $match: { stock: {$gte: 20}}
        },
        {
        $count: "enoughStock"
        }
    ]
)
// Average price of fruits on sale per supplier
db.fruits.aggregate(
    [
        {
        $match: { onSale: true}
        }, 
        {
            $group:
            {
                _id: "$supplier_id",
                avg_price: { $avg: "$price" },
            }
        }
    ]
 )

// Highest price of fruit per supplier 
db.fruits.aggregate(
    [
        {
        $group:
            {
            _id: "$supplier_id",
            max_price: {$max: "$price"}
            }
        }
    ]
)

// Lowest price of fruit per supplier 
db.fruits.aggregate(
    [
        {
        $group:
            {
            _id: "$supplier_id",
            min_price: {$min: "$price"}
            }
        }
    ]
)